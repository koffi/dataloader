﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using Google.Apis.Analytics.v3;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using System.Globalization;
using Google.Apis.Analytics.v3.Data;
using System.Security.Cryptography.X509Certificates;
using DataLoaderConsole.Modele;



namespace DataLoaderConsole
{
    public class GA
    {
        #region Init_to_be_moved_to_webconfig
        SqlConnection Conn = new SqlConnection("Persist Security Info=False;User id=sa; Password=S@Us3r!; Initial Catalog=STAGING_GA;server=10.1.8.178");
        public static string keyFilePath = @"c:\PrivateKey.p12";    // Downloaded from https://console.developers.google.com
        public static string serviceAccountEmail = "account-1@uds-datamart.iam.gserviceaccount.com";  // found https://console.developers.google.com
        #endregion

        protected void execute_sql(string SQL)
        {

            try
            {
                Conn.Open();

                SqlCommand cmd = new SqlCommand(SQL, Conn);
                cmd.ExecuteNonQuery();
                Conn.Close();

            }
            catch
            {

            }
            finally
            {
                Conn.Close();
            }

        }

        protected void InsertQueryData(int QueryId, string QueryName, IList<string> GAData)
        {

            try
            {

                string Columns = string.Empty;
                string TheValues = string.Empty;

                Conn.Open();

                SqlCommand cmd2 = new SqlCommand("Get_GA_Query_Columns", Conn);
                cmd2.CommandType = CommandType.StoredProcedure;
                cmd2.Parameters.AddWithValue("@QueryId", QueryId);

                SqlDataAdapter adapter = new SqlDataAdapter(); DataTable dt = new DataTable();
                adapter.SelectCommand = cmd2;
                adapter.Fill(dt);

                if (dt != null && dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        Columns += dt.Rows[i]["column_name"].ToString();

                        TheValues += "'" + GAData[i].ToString().Replace("'", "''") + "'";
                        if (i < dt.Rows.Count - 1) { Columns += ","; TheValues += ","; }
                    }
                }


                SqlCommand cmd = new SqlCommand("[dbo].[InsertQueryData]", Conn);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@QueryName", QueryName);
                cmd.Parameters.AddWithValue("@columns", Columns);
                cmd.Parameters.AddWithValue("@values", TheValues);
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Conn.Close();
            }

        }

        protected void InsertError(string query, string StartDate, string EndDate, string Message)
        {
            try
            {

                Conn.Open();



                SqlCommand cmd = new SqlCommand("[dbo].[InsertError]", Conn);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@QueryName", query);
                cmd.Parameters.AddWithValue("@StartDate", StartDate);
                cmd.Parameters.AddWithValue("@EndDate", EndDate);
                cmd.Parameters.AddWithValue("@Error", Message);

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Conn.Close();
            }
        
        }

        protected string GA_GetMetrics(int query_id)
        {
            string theMetrics = string.Empty;

            try
            {
                Conn.Open();

                SqlCommand cmd = new SqlCommand("Get_GA_Query_Metrics", Conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@QueryId", query_id);

                SqlDataAdapter adapter = new SqlDataAdapter(); DataTable dt = new DataTable();
                adapter.SelectCommand = cmd;
                adapter.Fill(dt);

                if (dt != null && dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        theMetrics += "ga:" + dt.Rows[i]["metric_name"].ToString();
                        if (i < dt.Rows.Count - 1) theMetrics += ",";
                    }
                }

                return theMetrics;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Conn.Close();
            }

        }

        protected string GA_GetDimensions(int query_id)
        {
            string theMetrics = string.Empty;

            try
            {
                Conn.Open();

                SqlCommand cmd = new SqlCommand("Get_GA_Query_Dimensions", Conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@QueryId", query_id);

                SqlDataAdapter adapter = new SqlDataAdapter(); DataTable dt = new DataTable();
                adapter.SelectCommand = cmd;
                adapter.Fill(dt);

                if (dt != null && dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        theMetrics += "ga:" + dt.Rows[i]["dim_name"].ToString();
                        if (i < dt.Rows.Count - 1) theMetrics += ",";
                    }
                }

                return theMetrics;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Conn.Close();
            }

        }

        protected void GA_Generate_GA_table(string query_name, int query_id)
        {

            string Columns = string.Empty;

            try
            {
                Conn.Open();

                SqlCommand cmd = new SqlCommand("Get_GA_Query_Columns", Conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@QueryId", query_id);

                SqlDataAdapter adapter = new SqlDataAdapter(); DataTable dt = new DataTable();
                adapter.SelectCommand = cmd;
                adapter.Fill(dt);

                if (dt != null && dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        Columns += dt.Rows[i]["column_name"].ToString() + " " + dt.Rows[i]["column_type"].ToString();
                        if (i < dt.Rows.Count - 1) Columns += ",";
                    }
                }

                SqlCommand cmd2 = new SqlCommand("[dbo].[GA_Generate_GA_table]", Conn);

                cmd2.CommandType = CommandType.StoredProcedure;

                cmd2.Parameters.AddWithValue("@tablename", query_name);
                cmd2.Parameters.AddWithValue("@columns", Columns);
                cmd2.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Conn.Close();
            }

        }

        public void ClearGAData(string query)
        {

            try
            {


                SqlCommand cmd = new SqlCommand("[dbo].[ClearData]", Conn);
                cmd.Parameters.AddWithValue("@queryname", query);
                Conn.Open();
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Conn.Close();
            }

        }

        public  DataTable GAQueries()
        {
            try
            {
                Conn.Open();

                SqlCommand cmd = new SqlCommand("GA_Get_queries", Conn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter adapter = new SqlDataAdapter(); DataTable dt = new DataTable();
                adapter.SelectCommand = cmd;
                adapter.Fill(dt);

                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Conn.Close();
            }
        }

        public DataTable GAScheduledQueries()
        {
            try
            {
                Conn.Open();

                SqlCommand cmd = new SqlCommand("GA_Get_Scheduled_queries", Conn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter adapter = new SqlDataAdapter(); DataTable dt = new DataTable();
                adapter.SelectCommand = cmd;
                adapter.Fill(dt);

                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Conn.Close();
            }
        }

        public IEnumerable<DateTime> EachDay(DateTime from, DateTime thru)
        {
            for (var day = from.Date; day.Date <= thru.Date; day = day.AddDays(1))
                yield return day;
        }

        static IEnumerable<DateTime> monthsBetween(DateTime d0, DateTime d1)
        {
            return Enumerable.Range(0, (d1.Year - d0.Year) * 12 + (d1.Month - d0.Month + 1))
                             .Select(m => new DateTime(d0.Year, d0.Month, 1).AddMonths(m));
        }

        public GA_queries GetGAQuerybyName(string QueryName)
        {

            // Get list of surveys that were already imported

            try
            {
                Conn.Open();

                SqlCommand cmd = new SqlCommand("Get_GA_Query_byName", Conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@QueryName", QueryName);

                SqlDataAdapter adapter = new SqlDataAdapter(); DataTable dt = new DataTable();
                adapter.SelectCommand = cmd;
                adapter.Fill(dt);

                GA_queries qury = new GA_queries();

                if (dt != null && dt.Rows.Count > 0)
                {
                    qury.query_id = Int32.Parse(dt.Rows[0]["query_id"].ToString());
                    qury.query_name = dt.Rows[0]["query_name"].ToString().TrimEnd('\r', '\n');
                    qury.query_startdate = dt.Rows[0]["query_startdate"].ToString();
                    qury.query_enddate = dt.Rows[0]["query_enddate"].ToString();
                    qury.query_sort = dt.Rows[0]["query_sort"].ToString();
                    qury.query_filters = dt.Rows[0]["query_filters"].ToString();
                    qury.query_max_result = dt.Rows[0]["query_max_result"].ToString();
                    qury.ga_ids = dt.Rows[0]["ga_ids"].ToString();
                }

                return qury;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Conn.Close();
            }

        }

        public void LoadGAData(string query)
        {

            GA_queries theQuery = GetGAQuerybyName(query);


            if (theQuery.query_name != string.Empty)
            {


                string[] scopes = new string[] { AnalyticsService.Scope.Analytics }; // view and manage your Google Analytics data

                //loading the Key file
                var certificate = new X509Certificate2(keyFilePath, "notasecret", X509KeyStorageFlags.Exportable);


                try
                {
                    theQuery.query_metrics = GA_GetMetrics(theQuery.query_id);
                    theQuery.query_dimensions = GA_GetDimensions(theQuery.query_id);



                    var credential = new ServiceAccountCredential(new ServiceAccountCredential.Initializer(serviceAccountEmail)
                    {
                        Scopes = scopes
                    }.FromCertificate(certificate));


                    AnalyticsService services = new AnalyticsService(new BaseClientService.Initializer()
                    {
                        HttpClientInitializer = credential,
                        ApplicationName = "UDS Datamart",
                    });


                    // get data day by day

                    DateTime TheStartDate = DateTime.ParseExact(theQuery.query_startdate, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                    DateTime TheEndDate = DateTime.ParseExact(theQuery.query_enddate, "yyyy-MM-dd", CultureInfo.InvariantCulture);

                    GA_Generate_GA_table(theQuery.query_name, theQuery.query_id);


                    foreach (DateTime day in EachDay(TheStartDate, TheEndDate))
                    {
                        var r = services.Data.Ga.Get(theQuery.ga_ids, day.ToString("yyyy-MM-dd"), day.ToString("yyyy-MM-dd"), theQuery.query_metrics);

                        //Specify some addition query parameters
                        r.Dimensions = theQuery.query_dimensions;
                        r.Sort = theQuery.query_sort;

                        if (theQuery.query_filters != null && theQuery.query_filters != "") { r.Filters = theQuery.query_filters; }

                        r.MaxResults = Int32.Parse(theQuery.query_max_result);
                        r.SamplingLevel = DataResource.GaResource.GetRequest.SamplingLevelEnum.HIGHERPRECISION; // "HIGHER_PRECISION";

                        //Execute and fetch the results of our query
                        Google.Apis.Analytics.v3.Data.GaData d = r.Execute();

                        //string Sql_insert = string.Empty;


                        if (d != null)
                        {
                            if (d.Rows != null)
                            {
                                foreach (IList<string> a in d.Rows)
                                {
                                    InsertQueryData(theQuery.query_id, theQuery.query_name, a);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }

        }

        public void LoadGAData(string query, string StartDate, string EndDate)
        {

            GA_queries theQuery = GetGAQuerybyName(query);


            if (theQuery.query_name != string.Empty)
            {


                string[] scopes = new string[] { AnalyticsService.Scope.Analytics }; // view and manage your Google Analytics data

                //loading the Key file
                var certificate = new X509Certificate2(keyFilePath, "notasecret", X509KeyStorageFlags.Exportable);


                try
                {
                    theQuery.query_metrics = GA_GetMetrics(theQuery.query_id);
                    theQuery.query_dimensions = GA_GetDimensions(theQuery.query_id);



                    var credential = new ServiceAccountCredential(new ServiceAccountCredential.Initializer(serviceAccountEmail)
                    {
                        Scopes = scopes
                    }.FromCertificate(certificate));


                    AnalyticsService services = new AnalyticsService(new BaseClientService.Initializer()
                    {
                        HttpClientInitializer = credential,
                        ApplicationName = "UDS Datamart",
                    });


                    // get data day by day

                    DateTime TheStartDate = DateTime.ParseExact(StartDate, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                    DateTime TheEndDate = DateTime.ParseExact(EndDate, "yyyy-MM-dd", CultureInfo.InvariantCulture);

                    GA_Generate_GA_table(theQuery.query_name, theQuery.query_id);


                    foreach (DateTime day in EachDay(TheStartDate, TheEndDate))
                    {
                        var r = services.Data.Ga.Get(theQuery.ga_ids, day.ToString("yyyy-MM-dd"), day.ToString("yyyy-MM-dd"), theQuery.query_metrics);

                        //Specify some addition query parameters
                        r.Dimensions = theQuery.query_dimensions;
                        r.Sort = theQuery.query_sort;

                        if (theQuery.query_filters != null && theQuery.query_filters != "") { r.Filters = theQuery.query_filters; }

                        r.MaxResults = Int32.Parse(theQuery.query_max_result);
                        r.SamplingLevel = DataResource.GaResource.GetRequest.SamplingLevelEnum.HIGHERPRECISION; // "HIGHER_PRECISION";

                        //Execute and fetch the results of our query
                        Google.Apis.Analytics.v3.Data.GaData d = r.Execute();

                        //string Sql_insert = string.Empty;


                        if (d != null)
                        {
                            if (d.Rows != null)
                            {
                                foreach (IList<string> a in d.Rows)
                                {
                                    InsertQueryData(theQuery.query_id, theQuery.query_name, a);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }

        }

        public void uploadGAPastWeekData(string query)
        {                
            
            string EndDate = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");
            string StartDate = DateTime.Now.AddDays(-7).ToString("yyyy-MM-dd");
            
            try
            {

                (new GA()).LoadGAData(query, StartDate, EndDate);
            }
            catch (Exception ex)
            {
                InsertError(query, StartDate, EndDate, ex.Message);
            }
        }

        public void uploadGAPreviousDayData(string query)
        {

            string PreviousDate = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");

            try
            {
                (new GA()).LoadGAData(query, PreviousDate, PreviousDate);
            }
            catch (Exception ex)
            {
                InsertError(query, PreviousDate, PreviousDate, ex.Message);
            }
        }

        public void LoadGADataByMonth(string query)
        {

            GA_queries theQuery = GetGAQuerybyName(query);


            if (theQuery.query_name != string.Empty)
            {


                string[] scopes = new string[] { AnalyticsService.Scope.Analytics }; // view and manage your Google Analytics data

                //loading the Key file
                var certificate = new X509Certificate2(keyFilePath, "notasecret", X509KeyStorageFlags.Exportable);


                try
                {
                    theQuery.query_metrics = GA_GetMetrics(theQuery.query_id);
                    theQuery.query_dimensions = GA_GetDimensions(theQuery.query_id);



                    var credential = new ServiceAccountCredential(new ServiceAccountCredential.Initializer(serviceAccountEmail)
                    {
                        Scopes = scopes
                    }.FromCertificate(certificate));


                    AnalyticsService services = new AnalyticsService(new BaseClientService.Initializer()
                    {
                        HttpClientInitializer = credential,
                        ApplicationName = "UDS Datamart",
                    });


                    // get data day by day

                    DateTime TheStartDate = DateTime.ParseExact(theQuery.query_startdate, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                    DateTime TheEndDate = DateTime.ParseExact(theQuery.query_enddate, "yyyy-MM-dd", CultureInfo.InvariantCulture);

                    GA_Generate_GA_table(theQuery.query_name, theQuery.query_id);


                    foreach (DateTime day in monthsBetween(TheStartDate, TheEndDate))
                    {
                        var r = services.Data.Ga.Get(theQuery.ga_ids, day.ToString("yyyy-MM-dd"), day.AddMonths(1).AddDays(-1).ToString("yyyy-MM-dd"), theQuery.query_metrics);

                        //Specify some addition query parameters
                        r.Dimensions = theQuery.query_dimensions;
                        r.Sort = theQuery.query_sort;

                        if (theQuery.query_filters != null && theQuery.query_filters != "") { r.Filters = theQuery.query_filters; }

                        r.MaxResults = Int32.Parse(theQuery.query_max_result);
                        r.SamplingLevel = DataResource.GaResource.GetRequest.SamplingLevelEnum.HIGHERPRECISION; // "HIGHER_PRECISION";

                        //Execute and fetch the results of our query
                        Google.Apis.Analytics.v3.Data.GaData d = r.Execute();

                        //string Sql_insert = string.Empty;


                        if (d != null)
                        {
                            if (d.Rows != null)
                            {
                                foreach (IList<string> a in d.Rows)
                                {
                                    InsertQueryData(theQuery.query_id, theQuery.query_name, a);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }

        }

        public void LoadGADataByMonth(string query, string StartDate, string EndDate)
        {

            GA_queries theQuery = GetGAQuerybyName(query);


            if (theQuery.query_name != string.Empty)
            {


                string[] scopes = new string[] { AnalyticsService.Scope.Analytics }; // view and manage your Google Analytics data

                //loading the Key file
                var certificate = new X509Certificate2(keyFilePath, "notasecret", X509KeyStorageFlags.Exportable);


                try
                {
                    theQuery.query_metrics = GA_GetMetrics(theQuery.query_id);
                    theQuery.query_dimensions = GA_GetDimensions(theQuery.query_id);



                    var credential = new ServiceAccountCredential(new ServiceAccountCredential.Initializer(serviceAccountEmail)
                    {
                        Scopes = scopes
                    }.FromCertificate(certificate));


                    AnalyticsService services = new AnalyticsService(new BaseClientService.Initializer()
                    {
                        HttpClientInitializer = credential,
                        ApplicationName = "UDS Datamart",
                    });


                    // get data day by day

                    DateTime TheStartDate = DateTime.ParseExact(StartDate, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                    DateTime TheEndDate = DateTime.ParseExact(EndDate, "yyyy-MM-dd", CultureInfo.InvariantCulture);

                    GA_Generate_GA_table(theQuery.query_name, theQuery.query_id);


                    foreach (DateTime day in monthsBetween(TheStartDate, TheEndDate))
                    {
                        var r = services.Data.Ga.Get(theQuery.ga_ids, day.ToString("yyyy-MM-dd"), day.AddMonths(1).AddDays(-1).ToString("yyyy-MM-dd"), theQuery.query_metrics);

                        //Specify some addition query parameters
                        r.Dimensions = theQuery.query_dimensions;
                        r.Sort = theQuery.query_sort;

                        if (theQuery.query_filters != null && theQuery.query_filters != "") { r.Filters = theQuery.query_filters; }

                        r.MaxResults = Int32.Parse(theQuery.query_max_result);
                        r.SamplingLevel = DataResource.GaResource.GetRequest.SamplingLevelEnum.HIGHERPRECISION; // "HIGHER_PRECISION";

                        //Execute and fetch the results of our query
                        Google.Apis.Analytics.v3.Data.GaData d = r.Execute();

                        //string Sql_insert = string.Empty;


                        if (d != null)
                        {
                            if (d.Rows != null)
                            {
                                foreach (IList<string> a in d.Rows)
                                {
                                    InsertQueryData(theQuery.query_id, theQuery.query_name, a);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }

        }

    }
}