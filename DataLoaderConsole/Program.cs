﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DataLoaderConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            GA ga_object = new GA();
            DataTable dt = ga_object.GAScheduledQueries();

            if (dt != null)
            { 
                foreach (DataRow row in dt.Rows)
                {
                     string queryname = row["query_name"].ToString();
                     if (queryname != null) ga_object.uploadGAPreviousDayData(queryname);
                }
            }

        }
    }
}
