﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoadGA.aspx.cs" Inherits="WebApplication1.GA_Import" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="style/bootstrap.css" rel="stylesheet" />
    <link href="style/styles.css" rel="stylesheet" />
    <link href="style/bootstrap.min.css" rel="stylesheet" />
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <script src="scripts/script.js"></script>

    <style type="text/css">
        .auto-style1 {
            font-size: x-small;
        }
        .auto-style2 {
            font-size: large;
        }
    </style>

</head>
<body>
<form id="form1" runat="server">
<div class="container">
<div class="col-lg-12">
<div class="row">
<div class="col-lg-4">   


<div id='cssmenu'>
<ul>
   <li><a href='#'><span>Home</span></a></li>
   <li class='active has-sub'><a href='#'><span>Survey Monkey</span></a>
      <ul>
         <li class='last'><asp:LinkButton ID="LinkButton_ImportSurveys" runat="server"  OnClick="LinkButton_ImportSurveys_Click"><span>Import Data</span></asp:LinkButton>
         </li>
         <li class='last'><a href='#'><span>Settings</span></a>
         </li>
      </ul>
   </li>
    <li class='active has-sub'><a href='#'><span>Google Analytics</span></a>
      <ul>
         <li class='last'><asp:LinkButton ID="LinkButton_ImportGA" runat="server"  OnClick="LinkButton_ImportGA_Click"><span>Import Data</span></asp:LinkButton>
         </li>
         <li class='last'><a href='#'><span>Settings</span></a>
         </li>
      </ul>
   </li>
   <li class='last'><a href='#'><span>About</span></a></li>
</ul>
</div>  
</div>
<div class="col-lg-6">
    <div>
        <h3>  GA DATA IMPORT </h3>

        <br />
        <br />
        <span class="auto-style2">Staging table name:</span>
        <asp:TextBox ID="TextBoxStaging" runat="server" Enabled="False"></asp:TextBox>
        <br />
        <span class="auto-style1">NB: queries in green already have been imported in staging.</span>

        <br />
&nbsp;<br />

        <br />
        <asp:GridView ID="GridViewSurveys" runat="server" AutoGenerateColumns="False"  OnRowDataBound = "GridViewSurveys_RowDataBound" Width="861px">
        <Columns>
            <asp:TemplateField>
                <ItemTemplate><asp:CheckBox ID="SurveySelector" runat="server" /></ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="query_id" HeaderText="Query ID" 
                SortExpression="query_id" >
            <ControlStyle Width="40%"/>
            </asp:BoundField>
            <asp:BoundField DataField="query_name" HeaderText="Query Name" 
                ReadOnly="True" SortExpression="query_name" >
            <ControlStyle Width="20%"/>
            </asp:BoundField>
            <asp:BoundField DataField="query_startdate" HeaderText="Start Date" 
                ReadOnly="True" SortExpression="query_startdate" >
            <ControlStyle Width="20%"/>
            </asp:BoundField>
            <asp:BoundField DataField="query_enddate" HeaderText="End Date" 
                ReadOnly="True" SortExpression="query_enddate" >      
            <ControlStyle Width="20%"/>
            </asp:BoundField>
        </Columns>
            <HeaderStyle BackColor="#0275D8" ForeColor="White" />
        </asp:GridView>
        <br />
        <br />
        <br />
        <asp:Button runat="server" ID="ButtonImportGAData" class="btn btn-primary"  OnClick="ButtonImportGAData_Click" Text="Execute Selected Query" />
    </div>

</div>     

</div>
</div>
</div>    
</form>
</body>
</html>
