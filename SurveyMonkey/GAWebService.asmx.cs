﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using GALibrary;
using System.Configuration;

namespace WebApplication1
{
    /// <summary>
    /// Summary description for GAWebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class GAWebService : System.Web.Services.WebService
    {
        string ConnectionString = ConfigurationManager.ConnectionStrings["SqlServices"].ToString();

        [WebMethod]
        public string uploadGAData(string query, string StartDate, string EndDate)
        {
            try
            {
                (new GA(ConnectionString)).LoadGAData(query, StartDate, EndDate);
                return "OK";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [WebMethod]
        public string uploadGAPastWeekData(string query)
        {
            try
            {
                string EndDate = DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd");
                string StartDate = DateTime.Now.AddDays(-7).ToString("yyyy-MM-dd");
                (new GA(ConnectionString)).LoadGAData(query, StartDate, EndDate);
                return "OK";
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
