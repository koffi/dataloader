﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//using SurveyMonkey;
using GALibrary;
using System.Data.SqlClient;
using System.Data;
using System.Globalization;
using System.Security.Cryptography.X509Certificates;
using System.Configuration;



namespace WebApplication1
{
    public partial class GA_Import : System.Web.UI.Page
    {

        public static string ConnectionString = ConfigurationManager.ConnectionStrings["SqlServicesGA"].ToString();

        GA TheGAObject = new GA(ConnectionString);

        protected void Page_Init(object sender, EventArgs e)
        {
            TextBoxStaging.Text = "FACT_GA";
            LoadGAQueries();
        }

        public void LoadGAQueries()
        {

            // Get list of surveys that were already imported

            //List<ExistingSurvey> ListOfExistingSurvey = new List<ExistingSurvey>();

            try
            {
                GridViewSurveys.DataSource = (TheGAObject.GAQueries()).DefaultView;
                GridViewSurveys.DataBind();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        // VIEWS

        protected void GridViewSurveys_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string count1 = e.Row.Cells[2].Text;
                string count2 = e.Row.Cells[3].Text;


                string setColorClass = string.Empty;

                if (count1 != count2)
                {
                    e.Row.BackColor = System.Drawing.Color.Beige; // setting green color class 
                }
                else
                {
                    e.Row.BackColor = System.Drawing.Color.GreenYellow; // setting green color class 
                }
            }
        }

        protected void ButtonImportGAData_Click(object sender, EventArgs e)
        {


            foreach (GridViewRow row in GridViewSurveys.Rows)
            {

                if (((CheckBox)row.FindControl("SurveySelector")).Checked)
                {
                    TheGAObject.LoadGAData(row.Cells[2].Text.ToLower());
                }

            }



        }

        protected void ButtonClearResults_Click(object sender, EventArgs e)
        {


            foreach (GridViewRow row in GridViewSurveys.Rows)
            {

                if (((CheckBox)row.FindControl("SurveySelector")).Checked)
                {
                    TheGAObject.ClearGAData(row.Cells[2].Text.ToLower());
                }

            }
        }

        protected void LinkButton_ImportSurveys_Click(object sender, EventArgs e)
        {
            Response.Redirect("LoadSurvey.aspx");
        }

        protected void LinkButton_ImportGA_Click(object sender, EventArgs e)
        {
            Response.Redirect("LoadGA.aspx");
        }

     }
}