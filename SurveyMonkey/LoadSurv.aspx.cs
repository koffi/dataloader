﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using SMLibrary;
using SMLibrary.Modele;

namespace WebApplication1
{
    public partial class LoadSurv : System.Web.UI.Page
    {

           

            protected void Page_Init(object sender, EventArgs e)
            {
                TextBoxStaging.Text = "FACT_SURVEY1";
                
                loadsurveys();

            }

            public void loadsurveys()
            {


                try
                {
                    // Get list of surveys that were already imported

                    List<ExistingSurvey> ListOfExistingSurvey = (new SM()).ListOfExistingSurvey();

                    GridViewSurveys.DataSource = ListOfExistingSurvey.OrderByDescending(x => x.DateCreated);
                    GridViewSurveys.DataBind();
                }
                catch (Exception ex)
                {
                    throw ex;
                }


            }

            protected void GridViewSurveys_RowDataBound(object sender, GridViewRowEventArgs e)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string count1 = e.Row.Cells[2].Text;
                    string count2 = e.Row.Cells[3].Text;


                    string setColorClass = string.Empty;

                    if (count1 != count2)
                    {
                        e.Row.BackColor = System.Drawing.Color.Beige; // setting green color class 
                    }
                    else
                    {
                        e.Row.BackColor = System.Drawing.Color.GreenYellow; // setting green color class 
                    }
                }
            }

            protected void ButtonImporSurveys_Click(object sender, EventArgs e)
            {



                List<string> ListOfSelectedSurvey = new List<string>();

                //for (int i = 0; i < CheckBoxListSurveys.Items.Count; i++)
                //{
                //    if (CheckBoxListSurveys.Items[i].Selected == true) ListOfSelectedSurvey.Add(CheckBoxListSurveys.Items[i].Value.ToString().ToLower());
                //}

                foreach (GridViewRow row in GridViewSurveys.Rows)
                {
                    if (((CheckBox)row.FindControl("SurveySelector")).Checked)
                    {
                        ListOfSelectedSurvey.Add(row.Cells[6].Text.ToLower());
                    }
                }

                if (ListOfSelectedSurvey != null)
                {
                    (new SM()).ImportSurveys(ListOfSelectedSurvey);

                }

                loadsurveys();

            }

            protected void ButtonClearSurveys_Click(object sender, EventArgs e)
            {

                foreach (GridViewRow row in GridViewSurveys.Rows)
                {

                    if (((CheckBox)row.FindControl("SurveySelector")).Checked)
                    {
                        (new SM()).CleanSurveyData(row.Cells[1].Text.ToLower());
                    }

                }

                loadsurveys();
            }

            protected void LinkButton_ImportSurveys_Click(object sender, EventArgs e)
            {
                Response.Redirect("LoadSurvey.aspx");
            }

            protected void LinkButton_ImportGA_Click(object sender, EventArgs e)
            {
                Response.Redirect("LoadGA.aspx");
            }
        
    }
}