﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoadSurvey.aspx.cs" Inherits="WebApplication1.LoadSurvey" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="style/bootstrap.css" rel="stylesheet" />
    <link href="style/styles.css" rel="stylesheet" />
    <link href="style/bootstrap.min.css" rel="stylesheet" />
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <script src="scripts/script.js"></script>

    <style type="text/css">
        .auto-style1 {
            font-size: x-small;
        }
        .auto-style2 {
            font-size: large;
        }
    </style>

</head>
<body>
<form id="form1" runat="server">
<div class="container">
<div class="col-lg-12">
<div class="row">
<div class="col-lg-4">
<div id='cssmenu'>
<ul>
   <li><a href='#'><span>Home</span></a></li>
   <li class='active has-sub'><a href='#'><span>Survey Monkey</span></a>
      <ul>
         <li class='last'><asp:LinkButton ID="LinkButton_ImportSurveys" runat="server"  OnClick="LinkButton_ImportSurveys_Click"><span>Import Data</span></asp:LinkButton>
         </li>
         <li class='last'><a href='#'><span>Settings</span></a>
         </li>
      </ul>
   </li>
    <li class='active has-sub'><a href='#'><span>Google Analytics</span></a>
      <ul>
         <li class='last'><asp:LinkButton ID="LinkButton_ImportGA" runat="server"  OnClick="LinkButton_ImportGA_Click"><span>Import Data</span></asp:LinkButton>
         </li>
         <li class='last'><a href='#'><span>Settings</span></a>
         </li>
      </ul>
   </li>
   <li class='last'><a href='#'><span>About</span></a></li>
</ul>
</div>  
</div>
<div class="col-lg-6">

    <div>
    
        <h3>
            SURVEY MONKEY DATA IMPORT
            </h3>
        <br />
        <br />
        <span class="auto-style2">Staging table name:</span>
        <asp:TextBox ID="TextBoxStaging" runat="server" Enabled="False"></asp:TextBox>
        <br />
        <span class="auto-style1">NB: Surveys in green have already been fully imported in staging.</span><br />
&nbsp;<br />

        <br />
        <asp:GridView ID="GridViewSurveys" runat="server" AutoGenerateColumns="False"  OnRowDataBound = "GridViewSurveys_RowDataBound" Width="861px">
        <Columns>
            <asp:TemplateField>
                <ItemTemplate><asp:CheckBox ID="SurveySelector" runat="server" /></ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="SurveyName" HeaderText="Survey Name" 
                SortExpression="SurveyName" >
            <ControlStyle Width="40%"/>
            </asp:BoundField>
            <asp:BoundField DataField="NbRowsAvail" HeaderText="Results available" 
                ReadOnly="True" SortExpression="NbRowsAvail" >
            <ControlStyle Width="20%"/>
            </asp:BoundField>
            <asp:BoundField DataField="NbRows" HeaderText="Results imported" 
                ReadOnly="True" SortExpression="NbRows" >
            <ControlStyle Width="20%"/>
            </asp:BoundField>
            <asp:BoundField DataField="IndivResp" HeaderText="individual responses" 
                ReadOnly="True" SortExpression="IndivResp" >
            <ControlStyle Width="20%"/>
            </asp:BoundField>
            <asp:BoundField DataField="DateCreated" HeaderText="Date created" 
                ReadOnly="True" SortExpression="DateCreated" >      
            <ControlStyle Width="20%"/>
            </asp:BoundField>
            <asp:BoundField DataField="SurveyId" HeaderText="Survey Id" 
                SortExpression="SurveyId" >
            <ControlStyle Width="40%"/>
            </asp:BoundField>
        </Columns>
            <HeaderStyle BackColor="#0275D8" ForeColor="White" />
        </asp:GridView>
        <br />
        <br />
        <br />
        <asp:Button runat="server" ID="ButtonImporSurveys" class="btn btn-primary"  OnClick="ButtonImporSurveys_Click" Text="Load Selected Surveys" />
        <asp:Button runat="server" ID="ButtonClearSurveys" class="btn btn-primary"  Text="Clear Selected Surveys" Width="245px" OnClick="ButtonClearSurveys_Click" />
    </div>

</div> 
</div>
</div>
</div>    
</form>
</body>
</html>
