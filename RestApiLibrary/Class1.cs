﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;

namespace RestApiLibrary
{

        public class DataObject
        {
            public string Name { get; set; }
        }

        public class BrightEdge
        {
            private const string URL = "https://api.brightedge.com/3.0/objects.accounts";
            private static string urlParameters = "?username=koffi@ualberta.ca&password=Kkgc2016";

            static void Main(string[] args)
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(URL);

                // Add an Accept header for JSON format.
                client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

                // List data response.
                HttpResponseMessage response = client.GetAsync(urlParameters).Result;
                if (response.IsSuccessStatusCode)
                {
                    // Parse the response body
                    var dataObjects = response.Content.ReadAsStringAsync().Result;
                    foreach (var d in dataObjects)
                    {
                        Console.WriteLine("{0}", d.ToString());
                    }
                }
                else
                {
                    Console.WriteLine("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
                }
            }
    }
}
