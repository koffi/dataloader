﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Google.Apis.Analytics.v3;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using System.Globalization;
using Google.Apis.Analytics.v3.Data;
using System.Security.Cryptography.X509Certificates;
using System.Data.SqlClient;




namespace WebApplication2
{
    public partial class Ga : System.Web.UI.Page
    {
        SqlConnection Conn = new SqlConnection("Persist Security Info=False;User id=sa; Password=S@Us3r!; Initial Catalog=STAGING_GA;server=129.128.199.62\\SQL2012");

        protected void Page_Load(object sender, EventArgs e)
        {

        }


        protected void execute_sql(string SQL) 
        {

            try
            {
                Conn.Open();

                SqlCommand cmd = new SqlCommand(SQL, Conn);
                cmd.ExecuteNonQuery();
                Conn.Close();

            }
            catch (Exception ex)
            {
                // TableCell tcell4 = new TableCell(); tcell4.Text = ex.Message;
            }
            finally
            {
                Conn.Close();
            }
        
        }


        protected void ButtonImportData_Click(object sender, EventArgs e)
        {


            string[] scopes = new string[] { AnalyticsService.Scope.Analytics }; // view and manage your Google Analytics data

            var keyFilePath = @"c:\PrivateKey.p12";    // Downloaded from https://console.developers.google.com
            var serviceAccountEmail = "account-1@uds-datamart.iam.gserviceaccount.com";  // found https://console.developers.google.com

            //loading the Key file
            var certificate = new X509Certificate2(keyFilePath, "notasecret", X509KeyStorageFlags.Exportable);


             try
            {

                var credential = new ServiceAccountCredential(new ServiceAccountCredential.Initializer(serviceAccountEmail)
                {
                    Scopes = scopes
                }.FromCertificate(certificate));


                AnalyticsService services = new AnalyticsService(new BaseClientService.Initializer()
                {
                 
                    HttpClientInitializer = credential,
                    ApplicationName = "UDS Datamart",
                
                });


                var r = services.Data.Ga.Get("ga:23407497", "2015-11-01", "2015-11-30", "ga:visits");

                //Specify some addition query parameters
                r.Dimensions = "ga:pagePath,ga:pageTitle";
                r.Sort = "-ga:visits";
                r.MaxResults = 10;


                //Execute and fetch the results of our query
                Google.Apis.Analytics.v3.Data.GaData d = r.Execute();





                string Sql_insert = string.Empty;


                if (d != null)
                {

                    // Clear tables
                    string sql0 = "IF OBJECT_ID('[PAGE_VIEWS]', 'U'  ) IS NOT NULL TRUNCATE TABLE  [PAGE_VIEWS] ;";
                    execute_sql(sql0);


                    foreach (var a in d.Rows) {
                                 Sql_insert = "insert into [PAGE_VIEWS] ([page_path],[page_title],[visits]) VALUES ('" + a[0] + "','" + a[1] +  "'," + a[2] + ")";
                                 execute_sql(Sql_insert);
                    } 
                }



            }
             catch (Exception ex)
             {

                 throw ex;

             }




        }

    }
}