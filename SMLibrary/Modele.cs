﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMLibrary.Modele
{

    public struct ExistingSurvey
    {
        public string SurveyId { get; set; }
        public string SurveyName { get; set; }
        public string NbRows { get; set; }
        public string NbRowsAvail { get; set; }
        public DateTime? DateCreated { get; set; }
        public string IndivResp { get; set; }
    }

    public struct SurveyResult
    {
        string _SurveyID;
        long? _RespondentID;
        long? _CollectorID;
        DateTime? _StartDate;
        DateTime? _DateModified;
        string _IPAddress;
        string _EmailAddress;
        string _FirstName;
        string _LastName;
        string _CourseraID;
        string _CourseName;
        int _SessionID;
        string _Question;
        string _QuestionType;
        string _Response;
        string _ResponseRange;


        public string SurveyID
        {
            get { return _SurveyID; }
            set { _SurveyID = value; }

        }

        public long? RespondentID
        {
            get { return _RespondentID; }
            set { _RespondentID = value; }

        }

        public string ResponseRange
        {
            get { return _ResponseRange; }
            set { _ResponseRange = value; }

        }

        public long? CollectorID
        {
            get { return _CollectorID; }
            set { _CollectorID = value; }

        }

        public DateTime? StartDate
        {
            get { return _StartDate; }
            set { _StartDate = value; }

        }

        public DateTime? DateModified
        {
            get { return _DateModified; }
            set { _DateModified = value; }

        }

        public string IPAddress
        {
            get { return _IPAddress; }
            set { _IPAddress = value; }

        }

        public string EmailAddress
        {
            get { return _EmailAddress; }
            set { _EmailAddress = value; }

        }

        public string FirstName
        {
            get { return _FirstName; }
            set { _FirstName = value; }

        }

        public string LastName
        {
            get { return _LastName; }
            set { _LastName = value; }

        }

        public string CourseraID
        {
            get { return _CourseraID; }
            set { _CourseraID = value; }

        }

        public int SessionID
        {
            get { return _SessionID; }
            set { _SessionID = value; }
        }

        public string CourseName
        {
            get { return _CourseName; }
            set { _CourseName = value; }

        }

        public string Question
        {
            get { return _Question; }
            set { _Question = value; }

        }
        public string QuestionType
        {
            get { return _QuestionType; }
            set { _QuestionType = value; }

        }

        public string Response
        {
            get { return _Response; }
            set { _Response = value; }

        }
    }

}