﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using SurveyMonkey;
using System.Data.SqlClient;
using System.Data;
using SurveyMonkey.Containers;
using System.Configuration;
using SMLibrary.Modele;

namespace SMLibrary
{


    public class SM
    {        
        public List<Survey> surveys = new List<Survey>();
        public static string apiKey = "";
        public static string token = "TRIN0Ag1PjouqSq.NxgFzv3I0yd46C7oxyENrUQ5u9lRnUiGPvOJj3kEiZIPwY6hHoHUHDj08ZRqv2aE4Zz6zpGoU9bstFZA7ZbGtOevvS3-Imw4.w.GgdZcUWHQiDWA";

        public static string ConnectionString = ConfigurationManager.ConnectionStrings["SqlServicesSurvey"].ToString();

        SqlConnection Conn = new SqlConnection(ConnectionString);

        public SurveyMonkeyApi sm = new SurveyMonkeyApi(apiKey, token);

        public  SM()
        {
            //surveys = sm.GetSurveyList().Where(w => (w.Title.ToString().ToUpper().Contains("MOOC") || w.Title.ToString().ToUpper().Contains("SPM")) && w.ResponseCount > 0).ToList(); 
            surveys = sm.GetSurveyList().Where(w => w.ResponseCount > 0).ToList(); 
        }

        protected void InsertSurvey(SurveyResult res)
        {

            try
            {

                SqlCommand cmd = new SqlCommand("[dbo].[InsertSurveyResponse]", Conn);
                Conn.Open();
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@SurveyID", res.SurveyID);
                cmd.Parameters.AddWithValue("@RespondentID", res.RespondentID);
                cmd.Parameters.AddWithValue("@CollectorID", res.CollectorID);
                cmd.Parameters.AddWithValue("@StartDate", res.StartDate);
                cmd.Parameters.AddWithValue("@DateModified", res.DateModified);
                cmd.Parameters.AddWithValue("@IPAddress", res.IPAddress);
                //cmd.Parameters.AddWithValue("@EmailAddress", res.EmailAddress);
                //cmd.Parameters.AddWithValue("@FirstName", res.FirstName);
                //cmd.Parameters.AddWithValue("@LastName", res.LastName);
                cmd.Parameters.AddWithValue("@Coursename", res.CourseName);
                cmd.Parameters.AddWithValue("@CourseraID", res.CourseraID);
                cmd.Parameters.AddWithValue("@SessionID", res.SessionID);
                cmd.Parameters.AddWithValue("@Question", res.Question);
                cmd.Parameters.AddWithValue("@QuestionType", res.QuestionType);
                cmd.Parameters.AddWithValue("@ResponseRange", res.ResponseRange);
                cmd.Parameters.AddWithValue("@Response", res.Response);

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Conn.Close();
            }

        }

        public void CleanSurveyData(string SurveyId)
        {

            try
            {


                SqlCommand cmd = new SqlCommand("[dbo].[CleanSurveyData]", Conn);
                Conn.Open();
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@SurveyID", SurveyId);

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Conn.Close();
            }

        }

        public  List<ExistingSurvey> ListOfExistingSurvey() 
        {

            try
            {
                List<ExistingSurvey> ExistingSurvey = new List<ExistingSurvey>();


                Conn.Open();

                SqlCommand cmd = new SqlCommand("CountExistingSurveyResponse", Conn);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adapter = new SqlDataAdapter(); 
                DataTable dt = new DataTable();
                adapter.SelectCommand = cmd;
                adapter.Fill(dt);


                foreach (var thesurvey in surveys)
                {
                    ExistingSurvey surv = new ExistingSurvey();
                    surv.SurveyName = thesurvey.Title;
                    surv.SurveyId = thesurvey.Id.ToString();
                    surv.NbRowsAvail = thesurvey.ResponseCount.ToString();

                    surv.DateCreated = thesurvey.DateCreated;


                    for (int i = 0; i <= dt.Rows.Count - 1; i++)
                    {
                        if (thesurvey.Title.ToUpper() == dt.Rows[i]["surveyid"].ToString().ToUpper())
                        {
                            surv.NbRows = dt.Rows[i]["NbRows"].ToString().ToUpper(); surv.IndivResp = dt.Rows[i]["individual_responses"].ToString().ToUpper(); break;
                        }
                    }

                    ExistingSurvey.Add(surv);
                }

                return ExistingSurvey;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Conn.Close();
            }
        }

        public void ImportSurveys(List<string> ListOfSelectedSurvey)
        {

            foreach (Survey aSurvey in surveys)
            {

                if (ListOfSelectedSurvey.Contains(aSurvey.Id.ToString().ToLower()))
                {

                    //TableRow trow = new TableRow();
                    //TableCell tcell1 = new TableCell(), tcell2 = new TableCell();

                    //tcell1.Text = aSurvey.Title; tcell2.Text = aSurvey.ResponseCount.ToString();
                    //trow.Cells.Add(tcell1); trow.Cells.Add(tcell2);

                    (new SM()).ImportaSurvey(aSurvey);

                }
            }
        }

        public void ImportaSurvey(Survey aSurvey)
        {
            Survey SurveyDetails = sm.GetSurveyDetails(aSurvey.Id ?? 0);

            // Get the Questions and Results 
            Survey SurveyResp = sm.PopulateSurveyResponseInformation(aSurvey.Id ?? 0);



            if (SurveyResp.Responses != null)
            {
                int response_counter = 0;
                foreach (Response r in SurveyResp.Responses)
                {
                    response_counter++;

                    foreach (ResponseQuestion rq in r.Questions)
                    {
                        Question qq = SurveyResp.Questions.Where(x => x.Id == rq.Id).FirstOrDefault();

                        var rr = rq.ProcessedAnswer.Response;

                        if (qq != null)
                        {
                            SurveyResult res = new SurveyResult();

                            if (qq.Headings[0] != null)
                            {
                                res.Question = qq.Headings[0].Heading;
                            }
                            else
                                res.Question = "Not Defined";

                            res.ResponseRange = "Not Defined";

                            res.CourseraID = "Not Defined";
                            res.SessionID = 0;
                            res.CourseName = "Not Defined"; 


                            if (r.CustomVariables != null || r.CustomVariables.Count >3)
                            {
                                if (r.CustomVariables.Count == 3)
                                { 
                                    if (r.CustomVariables["id"] != null) {res.CourseraID = r.CustomVariables["id"];} else {res.CourseraID = "Not Defined";};
                                    if (r.CustomVariables["s"] != null) { try { res.SessionID = Convert.ToInt32(r.CustomVariables["s"]); } catch (Exception e) { } } else res.SessionID = 0; 
                                    if (r.CustomVariables["course"] != null){ res.CourseName = r.CustomVariables["course"] ;} else res.CourseName = "Not Defined" ; 
                                }
                                else if (r.CustomVariables.Count == 2)
                                {
                                    if (r.CustomVariables["id"] != null) {
                                        res.CourseraID = r.CustomVariables["id"].Substring(0,40);
                                        res.CourseName = r.CustomVariables["id"].Substring(40);
                                    }

                                    if (r.CustomVariables["s"] != null) 
                                    {
                                        try
                                        {
                                            res.SessionID = Convert.ToInt32(r.CustomVariables["s"]);
                                        }
                                        catch (Exception e) { }
                                    } 
                                } 
                                else if (r.CustomVariables.Count == 1)
                                {
                                    if (r.CustomVariables.Keys.Contains("id")) if (r.CustomVariables["id"] != null)  res.CourseraID = r.CustomVariables["id"];
                                    if (r.CustomVariables.Keys.Contains("course")) if (r.CustomVariables["course"] != null) res.CourseName = r.CustomVariables["course"];
                                    if (r.CustomVariables.Keys.Contains("s")) if (r.CustomVariables["s"] != null) try { res.SessionID = Convert.ToInt32(r.CustomVariables["s"]); } catch (Exception e) { };
                                } 
                            }


                            if (SurveyResp.Title.Trim() == string.Empty) { res.SurveyID = SurveyResp.Nickname; } else res.SurveyID = SurveyResp.Title;
                            res.RespondentID = r.Id;
                            res.CollectorID = r.CollectorId;
                            res.StartDate = r.DateCreated;
                            res.DateModified = r.DateModified;
                            res.IPAddress = r.IpAddress;
                            res.QuestionType = rq.ProcessedAnswer.Response.GetType().ToString();
                            //res.EmailAddress = rq.ProcessedAnswer.Response;
                            //res.FirstName = r.FirstName;
                            //res.LastName = r.LastName;
                            res.Response = "Not Defined";


                            switch (rr.GetType().ToString())
                            {
                                    case "SurveyMonkey.ProcessedAnswers.SingleChoiceAnswer":
                                    {

                                        if (qq.Answers != null)
                                        {
                                            res.ResponseRange = String.Join(";", qq.Answers.Choices.Select(c => c.Text).ToList());
                                        }

                                        if (rr != null)
                                        {
                                            if (rr.GetType().GetProperty("Choice").GetValue(rr, null) != null) 
                                                res.Response = rr.GetType().GetProperty("Choice").GetValue(rr, null).ToString();
                                        }
                                        InsertSurvey(res);
                                    }
                                    break;
                                    case "SurveyMonkey.ProcessedAnswers.OpenEndedSingleAnswer":
                                    {

                                        if (qq.Answers != null)
                                        {
                                            res.ResponseRange = String.Join(";", qq.Answers.Choices.Select(c => c.Text).ToList());
                                        }

                                        if (rr != null)
                                        {
                                            if (rr.GetType().GetProperty("Text").GetValue(rr, null) != null)
                                                 res.Response = rr.GetType().GetProperty("Text").GetValue(rr, null).ToString();
                                        }
                                        InsertSurvey(res);
                                    }
                                    break;
                                    case "SurveyMonkey.ProcessedAnswers.MatrixSingleAnswer":
                                    {

                                        if (qq.Answers != null)
                                        {
                                            res.ResponseRange = String.Join(";", qq.Answers.Choices.Select(c => c.Text).ToList());
                                        }

                                        if (rr != null)
                                        {
                                            List<SurveyMonkey.ProcessedAnswers.MatrixSingleAnswerRow> response = (List<SurveyMonkey.ProcessedAnswers.MatrixSingleAnswerRow>)rr.GetType().GetProperty("Rows").GetValue(rr, null);

                                            foreach (SurveyMonkey.ProcessedAnswers.MatrixSingleAnswerRow i in response)
                                            {

                                                res.Question = i.RowName.ToString();
                                                res.QuestionType = rq.ProcessedAnswer.Response.GetType().ToString();
                                                res.Response = i.Choice.ToString();

                                                InsertSurvey(res);
                                            }
                                        }
                                    }
                                    break;
                                //    case "SurveyMonkey.ProcessedAnswers.MatrixRatingAnswer":
                                //    {

                                //        var rr = rq.ProcessedAnswer.Response;
                                //        if (rr != null)
                                //        {
                                //            List<SurveyMonkey.MatrixRatingAnswerRow> response = (List<SurveyMonkey.MatrixRatingAnswerRow>)rr.GetType().GetProperty("Rows").GetValue(rr, null);

                                //            foreach (SurveyMonkey.MatrixRatingAnswerRow i in response)
                                //            {

                                //                SurveyResult res = new SurveyResult();
                                //                res.Question = i.RowName.ToString();
                                //                res.QuestionType = rq.ProcessedAnswer.Response.GetType().ToString();
                                //                res.ResponseRange = String.Join(";", qq.Answers.Where(c => c.Type.ToString().ToLower() == "col").Select(c => c.Text).ToList());

                                //                res.Response = i.Choice.ToString();

                                //                InsertSurvey(res);
                                //            }
                                //        }
                                //    }
                                // break;
                                    case "SurveyMonkey.ProcessedAnswers.MultipleChoiceAnswer":
                                    {


                                        if (qq.Answers != null)
                                        {
                                            res.ResponseRange = String.Join(";", qq.Answers.Choices.Select(c => c.Text).ToList());
                                        }

                                        if (rr != null)
                                        {
                                            List<string> response = null;

                                            if (rr.GetType().GetProperty("Choices").GetValue(rr, null) != null)
                                            { response = (List<string>)rr.GetType().GetProperty("Choices").GetValue(rr, null); }

                                            foreach (string i in response)
                                            {
                                                res.Response = i;
                                                InsertSurvey(res);
                                            }
                                        }
                                        else
                                        {
                                            InsertSurvey(res);
                                        }
                                    }
                                    break;
                                    case "SurveyMonkey.ProcessedAnswers.MatrixMultiAnswer":
                                    {
                                        if (qq.Answers != null)
                                        {
                                            res.ResponseRange = String.Join(";", qq.Answers.Choices.Select(c => c.Text).ToList());
                                        }

                                        if (rr != null)
                                        {
                                            List<string> response = null;

                                            if (rr.GetType().GetProperty("Choices").GetValue(rr, null) != null) 
                                                    response = (List<string>)rr.GetType().GetProperty("Choices").GetValue(rr, null);

                                            foreach (string i in response)
                                            {
                                                res.Response = i;
                                                InsertSurvey(res);
                                            }
                                        }
                                        else
                                        {
                                            InsertSurvey(res);
                                        }
                                    }
                                    break;
                                //case "SurveyMonkey.MatrixMenuAnswer": 
                                //    {
                                //        var rr = rq.ProcessedAnswer.Response;
                                //        if (rr != null)
                                //        {
                                //            Dictionary<long, SurveyMonkey.Containers.Row> rows = (Dictionary<long, SurveyMonkey.Containers.Row>)rr.GetType().GetProperty("Rows").GetValue(rr, null);
                                //            foreach (var aRow in rows)
                                //            {   
                                //                var RowQuestion = aRow.Value.RowName;
                                //                foreach ( var aCol in aRow.Value.Columns)
                                //                { 
                                //                    var ColQuestion = aCol.Value.ColumnName;
                                //                    var response = aCol.Value.Choice;
                                //                    SurveyResult res = new SurveyResult();
                                //                    if (aSurvey.Nickname.Trim() == string.Empty ) { res.SurveyID = aSurvey.TitleText; } else res.SurveyID = aSurvey.Nickname;
                                //                    res.RespondentID = r.RespondentId;
                                //                    res.CollectorID = r.Respondent.CollectorId;
                                //                    res.StartDate = r.Respondent.DateStart;
                                //                    res.DateModified = r.Respondent.DateModified;
                                //                    res.IPAddress = r.Respondent.IpAddress;
                                //                    res.EmailAddress = r.Respondent.Email;
                                //                    res.FirstName = r.Respondent.FirstName;
                                //                    res.LastName = r.Respondent.LastName;
                                //                    res.CourseraID = CId_response;
                                //                    res.Question = qq.Heading + ":"+ ColQuestion + "/" + RowQuestion;
                                //                    res.QuestionType = rq.ProcessedAnswer.Response.GetType().ToString();
                                //                    res.ResponseRange = String.Join(";", qq.Answers.Select(c => c.Text).ToList()); ;// String.Join(";", qq.Answers.Where(c => c.Type.ToString().ToLower() == "col").Select(c => c.Text).ToList());
                                //                    res.Response = response;
                                //                    InsertSurvey(res);
                                //                }
                                //            }
                                //        }
                                //    }
                                //    break; 

                                    case "SurveyMonkey.ProcessedAnswers.MatrixRankingAnswer":
                                    {

                                        if (qq.Answers != null)
                                        {
                                            res.ResponseRange = String.Join(";", qq.Answers.Choices.Select(c => c.Text).ToList());
                                        }

                                        if (rr != null)
                                        {
                                            List<string> response = null;
                                            if (rr.GetType().GetProperty("Choices") != null) 
                                                response = (List<string>)rr.GetType().GetProperty("Choices").GetValue(rr, null);

                                            foreach (string i in response)
                                            {
                                                res.Response = i;
                                                InsertSurvey(res);
                                            }
                                        }
                                        else
                                        {
                                            InsertSurvey(res);
                                        }

                                    }
                                    break;
                                    default:
                                    {
                                        if (qq.Answers != null && qq.Answers.Choices!=null)
                                        {
                                            res.ResponseRange = String.Join(";", qq.Answers.Choices.Select(c => c.Text).ToList());
                                        }

                                        if (rr != null)
                                        {
                                            if (rr.GetType().GetProperty("Choice")!=null)
                                            if (rr.GetType().GetProperty("Choice").GetValue(rr, null) != null) res.Response = rr.GetType().GetProperty("Choice").GetValue(rr, null).ToString();
                                        }

                                        InsertSurvey(res);
                                    }
                                    break;
                            }
                        }
                    }
                }
            }

        }

    }
}
