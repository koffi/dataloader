﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic.FileIO;
using System.IO;
using System.Configuration;

namespace CSVConsole
{
    class Program
    {



        static void Main(string[] args)
        {
             string ConnectionString = ConfigurationManager.ConnectionStrings["SqlServicesOnDemand"].ToString();
             SqlConnection Conn = new SqlConnection(ConnectionString);
            

            string[] TheTables = { "course_item_passing_states",
                                   "course_passing_states", 
                                   "course_item_types",
                                   "course_modules",
                                   "users", 
                                   "courses", 
                                   "course_grades", 
                                   "course_memberships", 
                                   "course_items", 
                                   "on_demand_session_memberships", 
                                   "on_demand_sessions",
                                   "course_progress_state_types",
                                   "course_progress",
                                   "users_courses__certificate_payments",
                                   "demographics_question_types",
                                   "demographics_questions",
                                   "demographics_choices",
                                   "demographics_answers"

                                 };


                                 
            foreach(string theTable in TheTables)
            {
                string[] TheFiles = Directory.GetFiles(@"E:\ONDEMAND_Rawdata", theTable + ".csv", System.IO.SearchOption.AllDirectories);

                    ClearGAData(theTable);

                    foreach (string f in TheFiles)
                    {
                        DataTable Dt = GetDataTabletFromCSVFile(f);
                        InsertDataIntoSQLServerUsingSQLBulkCopy(Dt, theTable);
                    }
            }
        }


        private static DataTable GetDataTabletFromCSVFile(string csv_file_path)
        {
            DataTable csvData = new DataTable();
            try
            {
              using(TextFieldParser csvReader = new TextFieldParser(csv_file_path))
                 {
                    csvReader.SetDelimiters(new string[] { "," });
                    csvReader.HasFieldsEnclosedInQuotes = true;
                    string[] colFields = csvReader.ReadFields();
                    foreach (string column in colFields)
                    {
                        DataColumn datecolumn = new DataColumn(column);
                        datecolumn.AllowDBNull = true;
                        csvData.Columns.Add(datecolumn);
                    }
                    while (!csvReader.EndOfData)
                    {
                        string[] fieldData = csvReader.ReadFields();
                        
                        //Making empty value as null
                        for (int i = 0; i < fieldData.Length; i++)
                        {
                            if (fieldData[i] == "")
                            {
                                fieldData[i] = null;
                            }
                        }
                        csvData.Rows.Add(fieldData);
                    }
                } 
                
            }
            catch (Exception ex)
            {
                InsertError(csv_file_path, ex.Message);
            }
            InsertError(csv_file_path, "the file " + csv_file_path + " has been parsed successfully.");
            return csvData;
        }


        static void InsertError(string table,  string Message)
        {
            string ConnectionString = ConfigurationManager.ConnectionStrings["SqlServicesOnDemand"].ToString();
            SqlConnection Conn = new SqlConnection(ConnectionString);

            try
            {
          
                Conn.Open();

                SqlCommand cmd = new SqlCommand("[dbo].[InsertError]", Conn);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@TableName", table);
                cmd.Parameters.AddWithValue("@Error", Message);

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Conn.Close();
            }

        }



        static void InsertDataIntoSQLServerUsingSQLBulkCopy(DataTable csvFileData, string theTable)
        {
            string ConnectionString = ConfigurationManager.ConnectionStrings["SqlServicesOnDemand"].ToString();
            SqlConnection Conn = new SqlConnection(ConnectionString);
            try { 
                    using (Conn)
                    {
                        Conn.Open();
                        using (SqlBulkCopy s = new SqlBulkCopy(Conn))
                        {
                            s.DestinationTableName = theTable;
                            foreach (var column in csvFileData.Columns)
                                s.ColumnMappings.Add(column.ToString(), column.ToString());
                            s.WriteToServer(csvFileData);
                            InsertError(theTable, "the file " + theTable + " has been inserted successfully");
                        }
                    }
            }
            catch (Exception ex)
            {
                InsertError(theTable, ex.Message);
            }
            finally
            {
                Conn.Close();
            }
        }

        static void ClearGAData(string theTable)
        {
            string ConnectionString = ConfigurationManager.ConnectionStrings["SqlServicesOnDemand"].ToString();
            SqlConnection Conn = new SqlConnection(ConnectionString);
            try
            {
                

                SqlCommand cmd = new SqlCommand("[dbo].[ClearData]", Conn);
                cmd.Parameters.AddWithValue("@theTable", theTable);
                Conn.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Conn.Close();
            }

        }

       

    }
}
