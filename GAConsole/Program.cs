﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using GALibrary;


namespace GAConsole
{
    class Program
    {
        static void Main(string[] args)
        {

            string ConnectionString = ConfigurationManager.ConnectionStrings["SqlServices"].ToString();

            //GA TheGAObject = new GA(ConnectionString);

            GA ga_object = new GA(ConnectionString);
            DataTable dt = ga_object.GAScheduledQueries();

            if (dt != null)
            {
                foreach (DataRow row in dt.Rows)
                {
                    string queryname = row["query_name"].ToString();
                    if (queryname != null) ga_object.uploadGALast10DaysData(queryname);
                }
            }

        }
    }
}
